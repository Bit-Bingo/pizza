$(function () {
	var $navWr = $('.js-nav-wr'),
		navPosition = $navWr.offset().top;
	if(!$navWr.length) return;

	var $nav = $navWr.find('.js-nav'),
		$navBtn = $navWr.find('.js-nav-btn');

	$navBtn.click(function() {
		$nav.toggleClass('active');
		return false;
	});

	$(window).scroll(function(){
	    if ($(window).scrollTop() >= navPosition) {
	       $navWr.addClass('_sticky');
	    }
	    else {
	       $navWr.removeClass('_sticky');
	    }
	});
});
